package com.example.project5.view;

import com.example.project5.model.Cell;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

/**
 * This is the View (UI) of the project. It draws the Model by listening to the
 * CellView that represents each Cell.
 */
public class WorldView extends View implements CellViewListener {

	protected Cell[][] grid;
	protected Paint paint = new Paint();
	
	/**
	  * Constructor. Takes the cells that will make up the world.
	  */
	public WorldView(Cell[][] cells, Context context) {
		super(context);
		this.setBackgroundColor(Color.DKGRAY);
		grid = cells;
	}
	
	/**
	  * Draws the grid.
	  */
	@Override
	public void onDraw(Canvas canvas) {
		for(int i = 0; i < grid.length; i++)
		{
			for(int j = 0; j < grid[1].length; j++)
			{
				paint.setColor(grid[i][j].getView().getColor());
				canvas.drawRect(grid[i][j].getView().getRect(), paint);
		}
	}
}
	/**
	  * When model changes, View redraws itself.
	  */
	@Override
	public void update() {
		invalidate();
	}
}