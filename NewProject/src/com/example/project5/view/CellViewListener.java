package com.example.project5.view;

/**
 * This is the interface that allows the View to listen to the CellView's (which are
 * part of the model) to see when it needs to update.
 */
public interface CellViewListener {
	/**
	 * This method tells the View to invalidate() itself.
	 */
	void update();	
}
