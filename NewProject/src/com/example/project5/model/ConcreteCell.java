package com.example.project5.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;


import android.graphics.Color;

/** 
 * A concrete implementation of the Cell interface.
 */
public class ConcreteCell implements Cell{
	
	private Random random = new Random();
	protected CellView cellView;
	protected ArrayList<Actor> occupants = new ArrayList<Actor>();
	protected ArrayList<Cell> neighbors = new ArrayList<Cell>();
	protected Semaphore sema = new Semaphore(1); //capacity of one actor per cell
	
	/**
	  * This method allows an actor to enter this cell.
	  */
	@Override
	public void enter(Actor actor) throws InterruptedException {
		Cell previous = actor.getCell();
		
		if(previous != this)
		if(sema.tryAcquire()) {
		
		if(previous != null) {
			previous.leave(actor);
		}
		if(cellView.getColor() == Color.DKGRAY) { cellView.setColor(Color.GREEN); }
		addOccupant(actor);
		actor.setCell(this);
		}
	}
	
	/**
	  * This method adds another cell as a neighbor to this cell.  The
	  * other cell does not automatically become a neighbor of this cell.
	  */
	@Override
	public void addNeighbor(Cell cell) {
		neighbors.add(cell);
	}

	/**
	  * This method adds the actor to the occupant list
	  */
	protected void addOccupant(Actor actor) { occupants.add(actor); }
	
	/**
	  * This method removes the actor from the occupant list
	  */
	protected void removeOccupant(Actor actor) { occupants.remove(actor); }
	
	/**
	   * This method is invoked when the controller is clicked, it fires an event
	   * to the model telling it to change.
	   */
	public void onClick() {
		if(cellView.getColor() == Color.YELLOW) //checks if vulnerable
		{
			cellView.setColor(Color.DKGRAY);
			occupants.get(0).kill();
			removeOccupant(occupants.get(0));
			sema.release();
		}
	}
	
	/**
	  * This method returns the list of cells that are neighbors of this cell.
	  */
	@Override
	public List<Cell> getNeighbors() {
		return neighbors;
	}
	
	/**
	   * This method returns the list of actors currently occupying this cell.
	   */
	@Override
	public List<Actor> getOccupants() {
		return occupants;
	}
	
	/**
	  * This method allows an actor to leave this cell.
	  */
	@Override
	public void leave(Actor actor) {
		cellView.setColor(Color.DKGRAY);
		sema.release();
	}
	
	/**
	   * This method returns the graphical view associated with this cell.
	   */
	@Override
	public CellView getView() {
		return cellView;
	}
	
	/**
	   * This method sets the graphical view associated with this cell.
	   */
	@Override
	public void setView(CellView cellView) {
		this.cellView = cellView;
	}
	
	/**
	   * This method returns a random neighbor of this cell.
	   */
	@Override
	public Cell randomNeighbor() {
		int size = neighbors.size();
		return size == 0 ? null : neighbors.get(random.nextInt(size));
	}

}
