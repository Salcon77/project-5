package com.example.project5.model;

import com.example.project5.view.CellViewListener;

import android.graphics.Color;
import android.graphics.Rect;

/**
 * A concrete implementation of the CellView interface.
 */
public class ConcreteCellView implements CellView {
	
	protected Rect rect; //the rectangle where this CellView is (coordinates)
	protected CellViewListener listener; //the View listens to the CellViews
	protected int color = Color.DKGRAY; //default color is Dark Gray

	/**
	 * Constructor. Sets the View as the Listener and the Rect that will define
	 * this CellView on the View.
	 */
	public ConcreteCellView(Rect rect, CellViewListener listener) {
		this.rect = rect;
		this.listener = listener; 
	}

	/**
	 * This method returns the color of this CellView.
	 */
	public int getColor() {
		return color;
	}
	
	/**
	 * This method sets the color that this CellView will be drawn in.
	 */
	@Override
	public void setColor(int color) {
		this.color = color;
		update();
	}
	
	/**
	 * This method returns the Rectangle that defines this CellView.
	 */
	@Override
	public Rect getRect() {
		return rect;
	}
	
	/**
	 * This method tells the CellViewListener (The View) to invalidate itself.
	 */
	@Override
	public void update() {
		listener.update();
	}
}
