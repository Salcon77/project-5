package com.example.project5.model;

import java.util.Random;


import android.graphics.Color;
import android.os.AsyncTask;

/**
 * A concrete implementation of the Actor interface.
 */
public class ConcreteActor extends AsyncTask<Object, Object, Object> implements Actor {

	protected Cell currentCell;

	/** Returns the cell a specific actor is occupying.*/
	@Override
	public Cell getCell() {
		return currentCell;
	}
	
	/** Sets the cell a specific actor is occupying*/
	@Override
	public void setCell(Cell cell) {
		currentCell = cell;
	}
	
	/** 
	 * Effectively "kills" an actor, stopping his movement
	 * and removing it from the world of cells.
	 */
	@Override
	public void kill() {
		cancel(true);
	}

	/** 
	 * The background thread for an Actor.
	 */
	@Override
	protected Object doInBackground(Object... params) {
		Random random = new Random();
		while(!isCancelled()) { //while Actor is not dead perform his thread.
		try {
			Thread.sleep(random.nextInt(2000)); //sleep for random time up to 2 secs
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(!isCancelled()) { publishProgress(); } //if actor is still alive perform his action
		}
		return null;
	}
	
	/** 
	 * The progress update is where the move (or vulnerability shift) of the actor
	 * is scheduled onto the UI thread.
	 */
	@Override
	protected void onProgressUpdate(Object... progress) {
		if(isCancelled()) return; //if actor was killed do not perform his update
		Cell target = currentCell.randomNeighbor();
		Random random = new Random();
		if(random.nextInt(100) > 33) //only attempts to move 1/3 of time
			try {
				target.enter(this); //tries to enter a cell
			} catch (InterruptedException e){}
		if(currentCell.getView().getColor() == Color.GREEN) {
			currentCell.getView().setColor(Color.YELLOW);
		}
		else if(currentCell.getView().getColor() == Color.YELLOW)
			currentCell.getView().setColor(Color.GREEN);
		if(isCancelled())
			currentCell.getView().setColor(Color.DKGRAY);
	}
}
