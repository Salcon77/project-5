package com.example.project5.model;

/**
 * This is a concrete implementation of the World Interface.
 * This class represents a world consisting of a rectangular grid
 * of cells.
 */
public class CellWorld implements World {

  private Cell[][] grid;

  /**
   * This constructor builds a CellWorld from a rectangular
   * grid of cells.  It automatically makes horizontally, vertically,
   * and diagonally adjacent cells neighbors of each other.
   */
  public CellWorld(final Cell[][] grid) {

    this.grid = grid;

    int rows = grid.length;
    int cols = grid[1].length;

    for (int i = 0; i < rows; i ++) {
      for (int j = 0; j < cols; j ++) {
        if (i > 0) {
          if (j > 0) grid[i][j].addNeighbor(grid[i-1][j-1]);
          grid[i][j].addNeighbor(grid[i-1][j]);
          if (j < cols - 1) grid[i][j].addNeighbor(grid[i-1][j+1]);
        }
        if (j > 0) grid[i][j].addNeighbor(grid[i][j-1]);
        if (j < cols - 1) grid[i][j].addNeighbor(grid[i][j+1]);
        if (i < rows - 1) {
          if (j > 0) grid[i][j].addNeighbor(grid[i+1][j-1]);
          grid[i][j].addNeighbor(grid[i+1][j]);
          if (j < cols - 1) grid[i][j].addNeighbor(grid[i+1][j+1]);
        }
      }
    }
  }

  /**
   * This method adds an actor to the cell at the given position.
   * It is the responsibility of the caller of this method to
   * make sure that there is space for the actor at the given
   * position.
   */
  public boolean addActor(Actor actor, int xpos, int ypos) {
    try {
    	if(grid[ypos][xpos].getOccupants().isEmpty())
    	{
    	  grid[ypos][xpos].enter(actor);
    	  return true;
    	}
    	return false;
    } catch (InterruptedException e) {
      throw new InternalError();    }
  }
}