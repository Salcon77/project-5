package com.example.project5.model;

/**
 * This interface represents the World. It has the ability to add actors.
 */
public interface World {
	
	  /**
	   * This method adds an actor to this world at the given position.
	   */
	  boolean addActor(Actor actor, int xpos, int ypos);
}
