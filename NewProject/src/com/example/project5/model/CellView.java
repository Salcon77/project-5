package com.example.project5.model;

import android.graphics.Rect;

/**
 * This interface represents the graphical view that represents a cell
 * in the View.
 */
public interface CellView {
	
	/**
	 * This method sets the color that this CellView will be drawn in.
	 */
	void setColor(int color);
	
	/**
	 * This method returns the color of this CellView.
	 */
	int getColor();
	
	/**
	 * This method returns the Rectangle that defines this CellView.
	 */
	Rect getRect();

	/**
	 * This method tells the CellViewListener (The View) to invalidate itself.
	 */
	void update();	
}
