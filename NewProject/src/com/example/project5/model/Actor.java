package com.example.project5.model;

/**
 * This interface represents an actor who can move around in
 * this world of cells.
 */
public interface Actor {

	/** Returns the cell a specific actor is occupying.*/
	Cell getCell();
	
	/** Sets the cell a specific actor is occupying*/
	void setCell(Cell cell);
	
	/** 
	 * Effectively "kills" an actor, stopping his movement
	 * and removing it from the world of cells.
	 */
	void kill();
}
