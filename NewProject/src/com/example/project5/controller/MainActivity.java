package com.example.project5.controller;

import java.util.ArrayList;
import java.util.Random;

import com.example.project5.R;
import com.example.project5.model.Actor;
import com.example.project5.model.Cell;
import com.example.project5.model.CellWorld;
import com.example.project5.model.ConcreteActor;
import com.example.project5.model.ConcreteCell;
import com.example.project5.model.ConcreteCellView;
import com.example.project5.model.World;
import com.example.project5.view.WorldView;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.Display;
import android.view.Menu;
import android.view.MotionEvent;

@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
public class MainActivity extends Activity {

	protected Cell[][] cells;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// get the width and height of the screen.
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y;
		
		// create cell grid according to dimensions
	    cells = new Cell[height/100][width/100];
	    for (int i = 0; i < cells.length; i ++) {
	      for (int j = 0; j < cells[1].length; j ++) {
	        cells[i][j] = new ConcreteCell();
	      }
	    }
	    // create view, also finish building model
	    WorldView view = new WorldView(cells, this);
	    final int rows = cells.length;
		final int cols = cells[1].length;
		int leftx = 0;
		int rightx = 100;
		int topy = 0;
		int boty = 100;
		for (int i = 0; i < rows; i ++) {
		     for (int j = 0; j < cols; j ++) {
		    	cells[i][j].setView(new ConcreteCellView(new Rect(leftx, topy, rightx, boty), view));
				leftx = rightx;
				rightx += 100;
		     }
		     topy = boty;
			 boty += 100;
			 leftx = 0;
			 rightx = 100;
		 }
	    
	    // create world
	    World world = new CellWorld(cells);
	    setContentView(view);
	    
	    // create actors and populate world
	    int k = 5; //number of actors to add
	    Random random = new Random();
	    ArrayList<ConcreteActor> actors = new ArrayList<ConcreteActor>();
	    for(int i = 0; i < k; i++)
	    {
	    	Actor actor = new ConcreteActor();
	    	actors.add((ConcreteActor) actor);
	    	if(!world.addActor(actor, random.nextInt(cells[1].length), random.nextInt(cells.length)))
	    	{
	    		i--;
	    	}
	    }
	   
	    // start actors
	    for (ConcreteActor a : actors) {
	       a.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,  a);
	    }
	    
	    
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/**
	  * The way controller tells model to change.
	  */
	public boolean onTouchEvent(MotionEvent e) 
	{
		int x = (int) e.getX();
		int y = (int) e.getY();
		if(y/100 > cells.length || x/100 > cells[1].length) return false;
		//System.out.println("y: " + (y/100-1));
		//System.out.println("x: " + x/100);
		cells[y/100 - 1][x/100].onClick();
		return true;
	}
}
